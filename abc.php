<?php
$method = 'GET';
$url = 'http://192.168.18.224:3000/api/appinazor/osdevices.json';

$data = array(
	'appinazor_osdevice' => array(
	'name' => md5(microtime())
));
$data = json_encode($data);

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	'Content-Type: application/json',
	'Content-Length: ' . strlen($data))
);


$server_output = curl_exec ($ch);

echo $server_output;

curl_close ($ch);
?>