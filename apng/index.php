<?php
require_once __DIR__.'/APNG_Creator.php';
$media_dir = $_SERVER['DOCUMENT_ROOT'].'/media';
$gif_file = $media_dir.'/Tiara-forehead.gif';
$gif_file = imagecreatefromgif($gif_file);

$width = 400;
$height = 200;
$font = 5;
$text = 'What the hell?';
// Font Size
$font_width = imagefontwidth($font);
$font_height = imagefontwidth($font);

/*
-----------
Text Width
-----------
*/

$text_width = $font_width * strlen($text);

// Position to align in center
$position_center = ceil(($width - $text_width) / 2);

/*
-----------
Text Height
-----------
*/

$text_height = $font_height;

// Position to align in abs middle
$position_middle = ceil(($height - $text_height) / 2);

$default_image = imagecreatetruecolor($width, $height);
$background = imagecolorallocate($default_image, 0, 255, 255);
imagefill($default_image, 0, 0, $background);
$color = imagecolorallocate($default_image, 255, 0, 0);
imagestring($default_image, $font, $position_center, $position_middle, $text, $color);


$frame1 = imagecreatetruecolor($width, $height);
imagefill($default_image, 0, 0, $background);
$color = imagecolorallocate($frame1, 20, 255, 20);
$background = imagecolorallocate($frame1, 255, 0, 0);
imagefill($frame1, 0, 0, $background);
imagestring($frame1, $font, $position_center, $position_middle, $text, $color);

// Creating the animation
$animation = new APNG_Creator();
$animation->save_alpha = false;
$animation->save_time = false;

$animation->add_image($default_image, null, 0, 0, 0, false); // this image won't be the part of the animation
$animation->add_image($frame1, null, 1000, 0, 0);
$animation->save(__DIR__."/results/browser_test.png");
$animation->destroy_images();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<img src="results/browser_test.png" />
<!-- <img src="chompy2.png" /> -->
<img src="blur_animation.png" />
</body>
</html>
